.test:
  stage: test
  image: "$BUILD_IMAGE_NAME"
  artifacts:
    when: on_failure
    paths:
      - tiller.log

.test-on-k3s:
  extends: [".test"]
  services:
    - name: registry.gitlab.com/gitlab-org/cluster-integration/test-utils/k3s-gitlab-ci/releases/$K3S_VERSION
      alias: k3s
  before_script:
    - apk add curl
    - curl k3s:8081 > kubeconfig
    - export KUBECONFIG=$(pwd)/kubeconfig
    - kubectl version
  parallel:
    matrix:
      - K3S_VERSION:
        - v1.16.15-k3s1
        - v1.17.13-k3s1
        - v1.18.10-k3s1
        - v1.19.3-k3s1

.app-tests:
  script:
    - helmfile --file $CI_PROJECT_DIR/test/managed-apps/gitlab-runner/helmfile.yaml repos
    - ./test/installation_tests

gitlabRunner:
  extends: [".test-on-k3s", ".app-tests"]
  variables:
    APP_NAME: gitlab-runner
    APP_ENV_PREFIX: GITLAB_RUNNER
    HELMFILE_CONFIG: |
      gitlabRunner:
        installed: true

shellcheck:
  stage: test
  image: koalaman/shellcheck-alpine:stable
  needs: []
  script:
    - |
      shellcheck src/bin/* test/managed-apps/gitlab-runner/verify

helm-2-guard:
  extends: [".test-on-k3s", ".app-tests"]
  variables:
    HELM_HOST: localhost:44134
    TILLER_NAMESPACE: gitlab-managed-apps
  script:
    # install Helm 2 release
    - gl-ensure-namespace "$TILLER_NAMESPACE"
    - tiller -listen ${HELM_HOST} &
    - helm2 init --client-only
    - helm2 repo add gitlab https://charts.gitlab.io
    - helm2 repo update
    - helm2 install gitlab/gitlab-runner --name runner --namespace "$TILLER_NAMESPACE"
    - killall tiller
    # attempt detect Helm2 releases
    - gl-fail-if-helm2-releases-exist "$TILLER_NAMESPACE" | tee detect-releases.log || true
    # output should contain informative error message
    - grep -q "Helm v2 releases found in namespace 'gitlab-managed-apps'" detect-releases.log

gl-ensure-namespace:
  extends: [".test-on-k3s"]
  script:
    - gl-ensure-namespace foo-ns
    - kubectl get ns foo-ns
    - kubectl get ns foo-ns --show-labels | grep "app.gitlab.com/managed_by=gitlab"

gl-adopt-helpers:
  extends: [".test-on-k3s"]
  variables:
    HELM_HOST: localhost:44134
    TILLER_NAMESPACE: example-crds-ns
  script:
    - kubectl create namespace example-crds-ns
    - tiller -listen ${HELM_HOST} &
    - helm2 init --client-only
    - helm2 plugin install https://github.com/databus23/helm-diff
    - cd test/managed-apps/custom-chart
    # install helm v2 release
    - helmfile -b helm2 -f helmfile.yaml apply
    # ensure helm v3 release with adoption hooks works
    - helmfile -b helm3 -f helmfile-with-hooks.yaml apply
    # ensure helm v3 release works
    - helmfile -b helm3 -f helmfile.yaml apply
    # test that adopting a missing resource does not fail
    - gl-adopt-resource-with-helm-v3 --kind configmap --name does-not-exist --namespace example-crds-ns --release example-crds --release-namespace example-crds-ns
