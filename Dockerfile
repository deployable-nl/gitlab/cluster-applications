FROM "registry.gitlab.com/gitlab-org/cluster-integration/helm-install-image/releases/helm-2to3-2.17.0-3.7.1-kube-1.20.11-alpine-3.14"

ARG TARGETARCH

# Helm 3 is default, but we keep Helm 2 to fail pipelines early with an informative message
RUN ln -s /usr/bin/helm3 /usr/bin/helm

RUN wget https://github.com/roboll/helmfile/releases/download/v0.143.1/helmfile_linux_${TARGETARCH} \
  && mv helmfile_linux_${TARGETARCH} /usr/local/bin/helmfile \
  && chmod u+x /usr/local/bin/helmfile

# [make, go-1.14, golint] are dependencies for building helm-diff from source.
RUN apk add --no-cache bash make go \
  && go get -u golang.org/x/lint/golint
RUN cp $HOME/go/bin/golint /usr/local/bin/golint

# Builds helm-diff from source and removes dependencies afterwards.
#
# CGO_ENABLED=0 is necessary otherwise the build fails for ARM64
RUN wget https://github.com/databus23/helm-diff/archive/refs/heads/master.zip \
  && unzip master.zip && rm master.zip \
  && cd helm-diff-master \
  && GOARCH=${TARGETARCH} CGO_ENABLED=0 make install/helm3 \
  && cd .. \
  && rm -rf helm-diff-master \
  && rm /usr/local/bin/golint \
  && apk del make go

RUN helm plugin install https://github.com/aslafy-z/helm-git.git --version 0.10.0

COPY src/ /opt/cluster-applications/

RUN ln -s /opt/cluster-applications/bin/* /usr/local/bin/
